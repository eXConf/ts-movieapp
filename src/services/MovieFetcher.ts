import { IApiConfig, INetMovie, IMovie } from '../interfaces';
import mapper from '../helpers/movieMapper';
import MovieCategory from '../types/MovieCategory';

export default class MovieFetcher {
    api: IApiConfig;

    constructor(apiConfig: IApiConfig) {
        this.api = apiConfig;
    }

    async getByName(page = 1, query: string): Promise<IMovie[]> {
        return this.getMovies('search', page, query);
    }

    async getPopular(page = 1): Promise<IMovie[]> {
        return this.getMovies('popular', page);
    }

    async getUpcoming(page = 1): Promise<IMovie[]> {
        return this.getMovies('upcoming', page);
    }

    async getTopRated(page = 1): Promise<IMovie[]> {
        return this.getMovies('top-rated', page);
    }

    async getByID(id: number): Promise<IMovie> {
        const { baseUrl, details, key } = this.api;
        const response = await fetch(
            baseUrl + details + id + `?api_key=${key}`
        );

        const netMovie: INetMovie = await response.json();
        const [movie] = mapper([netMovie]);

        return movie;
    }

    async getMovies(
        movieCategory: MovieCategory,
        page = 1,
        query = ''
    ): Promise<IMovie[]> {
        const { baseUrl, popular, upcoming, topRated, search, key } = this.api;
        let category = '';
        let searchQuery = '';

        switch (movieCategory) {
            case 'popular':
                category = popular;
                break;

            case 'upcoming':
                category = upcoming;
                break;

            case 'top-rated':
                category = topRated;
                break;

            case 'search':
                category = search;
                searchQuery = `&query=${query}`;
                break;

            default:
                break;
        }

        const response = await fetch(
            baseUrl +
                category +
                `?api_key=${key}` +
                `&page=${page}` +
                `${searchQuery}`
        );

        const { results }: { results: INetMovie[] } = await response.json();
        const movies: IMovie[] = mapper(results);
        return movies;
    }
}
