import { IMovie } from '../interfaces';
import MovieCategory from '../types/MovieCategory';
import MovieFetcher from './MovieFetcher';
import apiConfig from '../data/apiConfig';
import {
    readFromLocalStorage,
    writeToLocalStorage,
} from '../helpers/localstorageHelper';
import { createElement, createSvgElement } from '../helpers/domHelper';

class MovieRenderer {
    container: HTMLElement | null;
    fetcher: MovieFetcher;
    category: MovieCategory;
    currentPage: number;
    currentQuery: string;
    isRandomExist: boolean;
    favorites: number[];

    constructor() {
        this.container = document.getElementById('film-container');
        this.fetcher = new MovieFetcher(apiConfig);
        this.category = 'popular';
        this.currentPage = 1;
        this.currentQuery = '';
        this.isRandomExist = false;
        this.favorites = readFromLocalStorage();
    }

    setCategory(category: MovieCategory): void {
        this.category = category;
    }

    setQuery(query: string): void {
        this.currentQuery = query;
    }

    async renderPopular(page = 1): Promise<void> {
        if (page === 1) this.clearMovies();

        const movies: IMovie[] = await this.fetcher.getPopular(page);

        this.renderMovies(movies);
    }

    async renderUpcoming(page = 1): Promise<void> {
        if (page === 1) this.clearMovies();

        const movies: IMovie[] = await this.fetcher.getUpcoming(page);

        this.renderMovies(movies);
    }

    async renderTopRated(page = 1): Promise<void> {
        if (page === 1) this.clearMovies();

        const movies: IMovie[] = await this.fetcher.getTopRated(page);

        this.renderMovies(movies);
    }

    async renderSearchResults(
        page = 1,
        query = this.currentQuery
    ): Promise<void> {
        if (!query) return;
        if (page === 1) this.clearMovies();

        const movies: IMovie[] = await this.fetcher.getByName(page, query);

        this.renderMovies(movies);
    }

    async renderMore(): Promise<void> {
        const page = (this.currentPage += 1);
        const { currentQuery, category } = this;

        switch (category) {
            case 'popular':
                this.renderPopular(page);
                break;

            case 'upcoming':
                this.renderUpcoming(page);
                break;

            case 'top-rated':
                this.renderTopRated(page);
                break;

            case 'search':
                this.renderSearchResults(page, currentQuery);
                break;

            default:
                break;
        }
    }

    renderRandom(movie: IMovie): void {
        const { title, overview, backdropPath } = movie;

        const container = document.getElementById('random-movie');
        const nameContainer = document.getElementById('random-movie-name');
        const descriptionContainer = document.getElementById(
            'random-movie-description'
        );

        if (!container || !nameContainer || !descriptionContainer) return;

        const bgImage = backdropPath
            ? apiConfig.backdropImageBaseUrl + backdropPath
            : './src/images/no-cover.jpg';

        container.style.backgroundImage = `url(${bgImage})`;
        container.style.backgroundSize = 'cover';
        container.style.backgroundRepeat = 'no-repeat';
        nameContainer.innerText = title;
        descriptionContainer.innerText = overview;
    }

    async renderFavorite(): Promise<void> {
        const favContainer = document.getElementById('favorite-movies');

        if (!favContainer) return;

        favContainer.innerHTML = '';

        this.favorites.forEach(async (favID) => {
            const movie = await this.fetcher.getByID(favID);
            const movieElement = this.createMovieElement(movie, false);

            favContainer?.appendChild(movieElement);
        });
    }

    renderMovies(movies: IMovie[]): void {
        movies.forEach((movie) => {
            const movieElement = this.createMovieElement(movie);

            this.container?.appendChild(movieElement);
        });

        if (!this.isRandomExist) {
            const randomIndex = Math.floor(Math.random() * movies.length);
            const randomMovie = movies[randomIndex];

            this.renderRandom(randomMovie);
            this.isRandomExist = true;
        }
    }

    createMovieElement(movie: IMovie, isMainScreen = true): HTMLElement {
        const { id, overview, posterPath, releaseDate } = movie;

        const wrapper = createWrapper(isMainScreen);
        const card = createCard();
        const img = createImg(apiConfig.cardImageBaseUrl, posterPath);
        const svg = createSVG(this.favorites);
        const path = createPath();
        const body = createBody();
        const text = createText(overview);
        const dateContainer = createDateContainer();
        const date = createDate(releaseDate);

        wrapper.appendChild(card);
        card.appendChild(img);
        card.appendChild(svg);
        svg.appendChild(path);
        card.appendChild(body);
        body.appendChild(text);
        body.appendChild(dateContainer);
        dateContainer.appendChild(date);

        function createWrapper(isMainScreen: boolean): HTMLElement {
            return createElement({
                tagName: 'div',
                className: `${
                    isMainScreen ? 'col-lg-3 col-md-4' : ''
                } col-12 p-2`,
            });
        }

        function createCard(): HTMLElement {
            return createElement({
                tagName: 'div',
                className: 'card shadow-sm',
            });
        }

        function createImg(
            cardImageBaseUrl: string,
            posterPath: string
        ): HTMLElement {
            const imgSrc = posterPath
                ? cardImageBaseUrl + posterPath
                : './src/images/no-poster.jpg';

            return createElement({
                tagName: 'img',
                attributes: {
                    src: imgSrc,
                },
            });
        }

        function createSVG(favorites: number[]): SVGElement {
            const fill = favorites.includes(id) ? 'red' : '#ff000078';
            const svg = createSvgElement({
                tagName: 'svg',
                className: 'bi bi-heart-fill position-absolute p-2',
                attributes: {
                    xmlns: 'http://www.w3.org/2000/svg',
                    stroke: 'red',
                    fill: fill,
                    width: '50',
                    height: '50',
                    viewBox: '0 -2 18 22',
                    'data-id': `${id}`,
                },
            });

            svg.addEventListener('click', () => {
                const id = parseInt(svg.getAttribute('data-id') as string, 10);
                const isFavored = favorites.includes(id);

                if (isFavored) {
                    favorites.splice(favorites.indexOf(id), 1)
                    svg.setAttribute('fill', '#ff000078');
                } else {
                    favorites.push(id);
                    svg.setAttribute('fill', 'red');
                }
                writeToLocalStorage(favorites);
            });

            return svg;
        }

        function createPath(): SVGElement {
            return createSvgElement({
                tagName: 'path',
                attributes: {
                    'fill-rule': 'evenodd',
                    d: 'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z',
                },
            });
        }

        function createBody(): HTMLElement {
            return createElement({
                tagName: 'div',
                className: 'card-body',
            });
        }

        function createText(overview: string): HTMLElement {
            const text = createElement({
                tagName: 'p',
                className: 'card-text truncate',
            });

            text.innerText = overview;

            return text;
        }

        function createDateContainer(): HTMLElement {
            return createElement({
                tagName: 'div',
                className: 'd-flex justify-content-between align-items-center',
            });
        }

        function createDate(releaseDate: string): HTMLElement {
            const date = createElement({
                tagName: 'small',
                className: 'text-muted',
            });

            date.innerText = releaseDate;

            return date;
        }

        return wrapper;
    }

    clearMovies(): void {
        if (this.container) {
            this.container.innerHTML = '';
        }
    }
}

export default MovieRenderer;
