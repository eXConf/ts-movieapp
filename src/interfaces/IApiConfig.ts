export default interface IApiConfig {
    key: string;
    baseUrl: string;
    cardImageBaseUrl: string;
    backdropImageBaseUrl: string;
    search: string;
    popular: string;
    topRated: string;
    upcoming: string;
    details: string;
}
