import IApiConfig from './IApiConfig';
import IMovie from './IMovie';
import INetMovie from './INetMovie';

export { IApiConfig, IMovie, INetMovie };
