export default interface IMovie {
    backdropPath: string;
    id: number;
    overview: string;
    posterPath: string;
    releaseDate: string;
    title: string;
}
