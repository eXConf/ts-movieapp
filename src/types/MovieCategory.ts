type MovieCategory = 'popular' | 'upcoming' | 'top-rated' | 'search';

export default MovieCategory;
