import { IApiConfig } from '../interfaces';

const config: IApiConfig = {
    key: '3eb48894fba78222bf9c0bf68f72127d',
    baseUrl: 'https://api.themoviedb.org/3',
    cardImageBaseUrl: 'https://image.tmdb.org/t/p/w500/',
    backdropImageBaseUrl: 'https://image.tmdb.org/t/p/w1280/',
    search: '/search/movie',
    popular: '/movie/popular',
    topRated: '/movie/top_rated',
    upcoming: '/movie/upcoming',
    details: '/movie/',
};

export default config;
