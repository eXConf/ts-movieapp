import MovieRenderer from './services/MovieRenderer';

export async function render(): Promise<void> {
    const movieRenderer = new MovieRenderer();

    const popularButton = document.getElementById('popular');
    const upcomingButton = document.getElementById('upcoming');
    const topRatedButton = document.getElementById('top_rated');
    const searchField = document.getElementById('search') as HTMLInputElement;
    const searchButton = document.getElementById('submit');
    const loadMore = document.getElementById('load-more');
    const sidebarBtn = document.querySelector('.navbar-toggler');

    movieRenderer.renderPopular();
    addListeners();

    function addListeners(): void {
        popularButton?.addEventListener('click', () => {
            movieRenderer.setCategory('popular');
            movieRenderer.renderPopular();
        });

        upcomingButton?.addEventListener('click', () => {
            movieRenderer.setCategory('upcoming');
            movieRenderer.renderUpcoming();
        });

        topRatedButton?.addEventListener('click', () => {
            movieRenderer.setCategory('top-rated');
            movieRenderer.renderTopRated();
        });

        sidebarBtn?.addEventListener('click', () => {
            movieRenderer.renderFavorite();
        });

        searchField?.addEventListener('keypress', (e) => {
            if (e.key === 'Enter') {
                makeSearch();
            }
        });

        searchButton?.addEventListener('click', () => {
            makeSearch();
        });

        loadMore?.addEventListener('click', () => {
            movieRenderer.renderMore();
        });
    }

    function makeSearch(): void {
        const query = searchField.value;
        if (!query) return;

        movieRenderer.setCategory('search');
        movieRenderer.setQuery(query);
        movieRenderer.renderSearchResults();

        searchField.value = '';
    }
}
