export function createElement({
    tagName,
    className,
    attributes = {},
}: {
    tagName: string;
    className?: string;
    attributes?: { [name: string]: string };
}): HTMLElement {
    const element = document.createElement(tagName);

    if (className) {
        const classNames = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    Object.keys(attributes).forEach((key) =>
        element.setAttribute(key, attributes[key])
    );

    return element;
}

export function createSvgElement({
    tagName,
    className,
    attributes = {},
}: {
    tagName: string;
    className?: string;
    attributes?: { [name: string]: string };
}): SVGElement {
    const element = document.createElementNS(
        'http://www.w3.org/2000/svg',
        tagName
    );

    if (className) {
        const classNames = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    Object.keys(attributes).forEach((key) =>
        element.setAttribute(key, attributes[key])
    );

    return element;
}
