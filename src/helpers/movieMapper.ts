import { INetMovie, IMovie } from '../interfaces';

const movieMapper = (movies: INetMovie[]): IMovie[] => {
    const mappedMovies: IMovie[] = movies.map((movie: INetMovie) => {
        const mappedMovie: IMovie = {
            id: movie.id,
            overview: movie.overview,
            backdropPath: movie.backdrop_path,
            posterPath: movie.poster_path,
            releaseDate: movie.release_date,
            title: movie.title,
        };

        return mappedMovie;
    });

    return mappedMovies;
};

export default movieMapper;
