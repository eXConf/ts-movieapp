const readFromLocalStorage = (): number[] => {
    const data = window.localStorage.getItem('favorite-movies');

    if (!data) return [];

    const ids = JSON.parse(data);
    return ids;
};

const writeToLocalStorage = (data: number[]): void => {
    window.localStorage.setItem('favorite-movies', JSON.stringify(data));
};

export { readFromLocalStorage, writeToLocalStorage };
